import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { HelpersService } from '../helpers.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  private email: string;
  private msg: string;
  constructor(private helper: HelpersService,private router: Router) { }

  ngOnInit() {
  }

  login(){
    this.helper.login$(this.email).subscribe( result => {
      if (result[0] != undefined) {
        this.router.navigate(['transactions'])
      } else {
        this.msg = "Wrong email, please check your emails"
      }
    })
  }

}
