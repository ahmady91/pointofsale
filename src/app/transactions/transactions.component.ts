import { Component, OnInit } from '@angular/core';
import { HelpersService } from '../helpers.service';

@Component({
  selector: 'app-transactions',
  templateUrl: './transactions.component.html',
  styleUrls: ['./transactions.component.css']
})
export class TransactionsComponent implements OnInit {
  private price: string;
  private paid: string;
  private remind: number;
  private msg1: string;
  private msg2: string;
  constructor(private helper: HelpersService) { }

  ngOnInit() {
  }

  calculate(){
  	this.helper.exchange$(this.price, this.paid).subscribe(
  		result => {
  			this.msg1 = "Bill created Successfully";
  		}
  	)
    this.remind = this.paid - this.price;
  }

  save(){
    this.helper.transactionLog$(this.price, this.paid).subscribe(
      result => {
        this.msg2 = "Transactions saved successfuly in database";
      }
    )
  }

}
