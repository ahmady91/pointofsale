import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from "@angular/common/http";
import { Router, Data } from '@angular/router';
import { Observable, of } from 'rxjs';
import { catchError, debounceTime, distinctUntilChanged, map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class HelpersService {
	private chasierID = "1";
	private bill = "id";

    constructor(private http: HttpClient, private router: Router) { }

    public login$(email: string) {
	    let header = new HttpHeaders().set('Content-Type', 'application/json');
	    let body = JSON.stringify({ "email": email });
	    let options = { headers: header };

	    return this.http.post<any>("/cashier/login", body, options).pipe(
	        debounceTime(200),
	        distinctUntilChanged(),
	        map(
	            res => {
	                let result = res;
	                if (result[0]) {
	                	sessionStorage.setItem(this.chasierID, result[0].id);	                	
	                }
	                return result;
	            }
	        ),
	    )
    }

    public exchange$(price1: string, paid1: string) {
	  	let header = new HttpHeaders().set('Content-Type', 'application/json');
	    let options = { headers: header };
	    let price = Number(price1);
  		let paid = Number(paid1);
  		let remain = paid - price;
  		let chasierID = Number(sessionStorage.getItem(this.chasierID));
  		let body2 = JSON.stringify([price,paid,remain,chasierID])
	    return this.http.post<any>("/transactions/createP", body2, options).pipe(
	      debounceTime(200),
	      distinctUntilChanged(),
	      map(
	        res => {
	          let result = res;
	          sessionStorage.setItem(this.bill, result.insertId);
	          return result.insertId;
	        }
	      ),
	    )	  	
	}

	public transactionLog$(price1: string, paid1: string) {
		let header = new HttpHeaders().set('Content-Type', 'application/json');
	    let options = { headers: header };
	    let productID = Number(sessionStorage.getItem(this.bill));
		let bills=[50,20,10,5,1];
		let price = Number(price1);
  		let paid = Number(paid1);
	  	let balance = 0;
	  	let remain = paid - price;
	  	let returnedChashObj ={};
	  	let resultText = "";
	  	let resultArray = [];
	  	while(remain!==0) {
	  	    for(let i=0;i<bills.length;i++) {
	  		    if(remain>=bills[i]) {
	  		      balance=bills[i];
	  		      break;  			
	            }
	  		}
	  		remain=remain-balance;
	    	if(returnedChashObj[balance]) {
	  			returnedChashObj[balance]+=1;
	  		} else {
	  			returnedChashObj[balance]=1;
	  		}
	    }
	    let chasierID = Number(sessionStorage.getItem(this.chasierID));
	    resultArray.push([chasierID,'Deposte',paid,1,productID]);
	    for(let key in returnedChashObj) {
	        resultArray.push([chasierID,'Withdraw',Number(key),returnedChashObj[key],productID])
	    }
	    let body = JSON.stringify(resultArray);
	    return this.http.post<any>("/transactions/createT", body, options).pipe(
	      debounceTime(200),
	      distinctUntilChanged(),
	      map(
	        res => {
	          let result = res;
	          return result;
	        }
	      ),
	    )
	}

	public cashiers$() {
	  	let header = new HttpHeaders().set('Content-Type', 'application/json');
	    let options = { headers: header };
	    return this.http.get<any>("/cashier", options).pipe(
	      debounceTime(200),
	      distinctUntilChanged(),
	      map(
	        res => {
	          let result = res;
	          return result;
	        }
	      ),
	    )	  	
	}

	public cashiersLogs$(id: number) {
	  	let header = new HttpHeaders().set('Content-Type', 'application/json');
	    let options = { headers: header };
	    return this.http.get<any>(`/transactions/retrive/`+id, options).pipe(
	      debounceTime(200),
	      distinctUntilChanged(),
	      map(
	        res => {
	          let result = res;
	          return result;
	        }
	      ),
	    )	  	
	}

	public cashiersProductsLogs$(id: number) {
	  	let header = new HttpHeaders().set('Content-Type', 'application/json');
	    let options = { headers: header };
	    return this.http.get<any>(`/transactions/retriveP/`+id, options).pipe(
	      debounceTime(200),
	      distinctUntilChanged(),
	      map(
	        res => {
	          let result = res;
	          return result;
	        }
	      ),
	    )	  	
	}

}
