import { Component, OnInit } from '@angular/core';
import { HelpersService } from '../helpers.service';
import * as moment from 'moment';

@Component({
  selector: 'app-cashier',
  templateUrl: './cashier.component.html',
  styleUrls: ['./cashier.component.css']
})
export class CashierComponent implements OnInit {
  public cashers: CasherData[];
  public cashInfo: CashLogs[];
  public productsLogs: ProductsLogs[];
  constructor(private helper: HelpersService) { }

  ngOnInit() {
	  this.helper.cashiers$().subscribe(
  		result => {
  			this.cashers = result
  		}
  	)
  }

  selectionChange(id): void {
    this.helper.cashiersLogs$(id).subscribe(
  		result => {
  			this.cashInfo = result
  			this.cashInfo.forEach(log=> {
  				log.time = moment(log.time).format("dddd, MMMM Do YYYY, h:mm:ss a");
  			})
  		}
  	)
    this.helper.cashiersProductsLogs$(id).subscribe(
      result => {
        this.productsLogs = result
        this.productsLogs.forEach(log=> {
          log.time = moment(log.time).format("dddd, MMMM Do YYYY, h:mm:ss a");
        })
      }
    )
}

interface CasherData {
	firstName: string;
	lastName: string;
	id: number;
	email: string;
}

interface CashLogs {
	id: number;
	time: string;
	cashierId: number;
	type: string;
	class:number;
	ammount:number;
	productId: number;
}

interface ProductsLogs {
  id: number;
  time: string;
  product_cost: number;
  paid_amount: number;
  change_amount:number;
  cashierId:number;
}