const mysql = require('mysql');
const connection = mysql.createConnection({
	host:'us-cdbr-iron-east-04.cleardb.net',
	user:'bd4c21c0307617',
	password:'7b2bb179',
	database: 'heroku_3e773d53d4c8976'
});

connection.connect(function(err){
	if(err){
		throw(err);
	}
	console.log('connected');
	// connection.query("CREATE DATABASE IF NOT EXISTS PoS ",function(err,result){
	// 	if(err) throw err;
	// 	console.log("database created")
	// })
	// connection.query("USE PoS ",function(err,result){
	// 	if(err) throw err;
	// 	console.log("connected to PoS database")
	// })
	connection.query("CREATE TABLE IF NOT EXISTS cashier (id INT AUTO_INCREMENT PRIMARY KEY,firstName VARCHAR(45) NOT NULL,lastName VARCHAR(45) NOT NULL,email VARCHAR(45) NOT NULL UNIQUE)",function(err,res){
		if(err) throw err;
		console.log('cashier table created')
	})
	connection.query("CREATE TABLE IF NOT EXISTS products_sell (id INT AUTO_INCREMENT PRIMARY KEY,time TIMESTAMP DEFAULT CURRENT_TIMESTAMP,product_cost INT NOT NULL,paid_amount INT NOT NULL,change_amount INT NOT NULL,cashierId INT NOT NULL,FOREIGN KEY (cashierId) REFERENCES cashier(id) )",function(err,res){
		if(err) throw err;
		console.log('products_sell table created')
	})
	connection.query("CREATE TABLE IF NOT EXISTS transactions (id INT AUTO_INCREMENT PRIMARY KEY,time TIMESTAMP DEFAULT CURRENT_TIMESTAMP,cashierId INT, type VARCHAR(20),class INT,ammount INT,productId INT NOT NULL,FOREIGN KEY (cashierId) REFERENCES cashier(id), FOREIGN KEY (productId) REFERENCES products_sell(id))",function(err,result){
		if(err) throw err;
		console.log('transactions table created');
	})
})

module.exports=connection;