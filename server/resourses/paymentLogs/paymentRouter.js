var paymentRouter=require('express').Router();
var paymentController = require('./paymentController');

paymentRouter.route('/createT')
.post(function(req,res){
	paymentController.createT(req,res);
})

paymentRouter.route('/createP')
.post(function(req,res){
	paymentController.createP(req,res);
})
module.exports=paymentRouter;

paymentRouter.route('/retrive/:cashierId')
.get(function(req,res){
	paymentController.retrive(req,res);
})

paymentRouter.route('/retriveP/:cashierId')
.get(function(req,res){
	paymentController.retriveP(req,res);
})
