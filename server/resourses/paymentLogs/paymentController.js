var connection =require('../../db');

// create transaction logs
exports.createT=function(req,res){
	var insert ='insert into transactions(cashierId,type,class,ammount,productId) values ?';
	var values=req.body;
	console.log(values)
	connection.query(insert,[values],function(err,result){
		if(err) throw err;
		res.send(result)
	});
}

//retrive all data for certian cashier
exports.retrive=function(req,res){
	var id=req.params.cashierId;
	var sql='select * from transactions where cashierId='+id;

	connection.query(sql,function(err,result){
		if(err) throw err;
		res.send(result)
	})
}

// create products sell logs
exports.createP=function(req,res){
	var insert ='insert into products_sell(product_cost,paid_amount,change_amount,cashierId) values (?)';
	var values=[req.body];
	connection.query(insert,values,function(err,result){
		if(err) throw err;
		res.send(result)
	});
}

exports.retriveP=function(req,res){
	var id=req.params.cashierId;
	var sql='select * from products_sell where cashierId='+id;

	connection.query(sql,function(err,result){
		if(err) throw err;
		res.send(result)
	})
}