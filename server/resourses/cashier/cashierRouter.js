var cashierRouter=require('express').Router();
var cashierController = require('./cashierController');

cashierRouter.route('/')
.get(function(req,res){
	cashierController.retrive(req,res);
})
.post(function (req, res) {
	cashierController.create(req,res);
})

cashierRouter.route('/login')
.post(function(req,res){
	cashierController.retriveOne(req,res);
})


module.exports=cashierRouter;
