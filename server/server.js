var express = require('express');
var path = require('path')
var bodyParser = require('body-parser');
var db = require('./db');

var cashierRouter=require('./resourses/cashier/cashierRouter');
var paymentRouter=require('./resourses/paymentLogs/paymentRouter');

var app=express();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }))
app.use(express.static(__dirname + '/../dist/codaCity'));

app.get('/',function(req,res){
	res.sendFile('index.html')
})

app.use('/cashier',cashierRouter)
app.use('/transactions',paymentRouter);
app.get('*', (req, res) => {
  res.sendFile(path.join(__dirname, '../dist/codaCity/index.html'));
});

module.exports=app;
