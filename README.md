#Point Of Sale - CodaCity

this is a log application for Point of sale systems, which allow cashier to add transaction and it will log it.

---

## Technologies:

* Front-end : Angular 6.
* Back-end : NodeJS ( Express ).
* Database : cleardb ( MySQL) .

---

## How to Run:

* clone this repo to your pc.

* run your terminal, then write :
	1. `$ npm install` .

	2. `$npm start` .

---

## Insert Cashier to Database:

there is no option to install cashier in DB from web.

you need to use Postman (recommended) or cURL and make a post request to `http://localhost:3000/cashier`

it must be in **application/json** type and its format as `['firstName','lastName','email']` .

---

## Auther:
- [Ahmad Alyaaqba](https://bitbucket.org/ahmady91/) - Auther -
